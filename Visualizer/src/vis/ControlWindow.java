package vis;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Math.*;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;

import com.jogamp.newt.event.WindowAdapter;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.gl2.GLUT;

public class ControlWindow implements GLEventListener {
	GLJPanel window;
	GLU glu;
	GLUT glut;
	static Genome input; 
	int WINDOW_WIDTH = 200;
	int WINDOW_HEIGHT = 200;
    static  ArrayList<Color> gradient = new ArrayList<Color>();
	static double[] controlArray;
	static HashMap<Double, Color> colorMap = new HashMap<Double, Color>();

    ControlWindow(Genome g){
        GLProfile glp = GLProfile.getDefault();
        GLCapabilities caps = new GLCapabilities(glp);
        // Create the OpenGL rendering canvas
        window = new GLJPanel(caps);

        input = g;
        window.addGLEventListener(this);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        gradient.add(new Color(255, 255, 255));
        gradient.add(new Color(255, 240, 0));
        gradient.add(new Color(255, 118, 0));
        gradient.add(new Color(253, 0, 0));
        gradient.add(new Color(0, 0, 0));
    }

    public void giveListener(InteractiveMediator m){

	}
	
	//return a color based on the supplied z value, based on the stats of the genome and the start and end colours of the gradient.
	public static Color getColor(double z){
		if(!colorMap.containsKey(z)){
			if(z>1) {
                colorMap.put(z, getNewColor((z - input.zDist.firstKey())/(input.zDist.lastKey() - input.zDist.firstKey())));
            }
			else
                colorMap.put(z, getNewColor(z));
		}
		return colorMap.get(z);
	}

    static Color getNewColor(double z){
         double space = 1.0 / (double) (gradient.size() - 1);
         int index = (int) floor(z / space);

         if(z<1.0){
         double red = lerp((double) gradient.get(index).getRed() / 255.0, (double) gradient.get(index+1).getRed()/255.0 , (z%space) * (gradient.size() - 1));
         double green = lerp((double) gradient.get(index).getGreen() /255.0 , (double) gradient.get(index+1).getGreen()/255.0 , (z%space) * (gradient.size() - 1));
         double blue = lerp((double) gradient.get(index).getBlue()/ 255.0 , (double) gradient.get(index+1).getBlue()/255.0 , (z%space) * (gradient.size() - 1));
          //if (z>0.8) System.out.println("space " + space + " index " + index + " z " + z + " red " + red + " green " + green + " blue " + blue);
             return new Color((int) (red*255), (int) (green*255), (int) (blue*255));
         }
         else{
             System.out.println("black");
             return gradient.get(gradient.size()-1);
         }
    }


    static double lerp(double v0, double v1, double x) {
        return v0+(v1-v0)*x;
    }


	@Override
	public void display(GLAutoDrawable drawable) {
	    GL2 gl = drawable.getGL().getGL2(); 
	    gl.glClear(GL.GL_COLOR_BUFFER_BIT);
	    renderGraph(gl);
	}

	
	private void renderGraph(GL2 gl){
		double stepSize = 1.8 / input.zDist.size();
		System.out.println(stepSize);
		ArrayList<Double> keyList = new ArrayList<Double>(input.zDist.keySet());
		double xVal = -0.9;
		double yMax = 0.5 / (double) input.maxZ;
		System.out.println("yMax : " + yMax);
        gl.glColor3d(0, 0, 0);
		gl.glBegin(GL2.GL_POINTS);
		for(int i=0; i<keyList.size(); i++){
			gl.glVertex2d(xVal, 0.1 + yMax * input.zDist.get(keyList.get(i)));
			xVal += stepSize;
		}
		gl.glEnd();
        gl.glColor4d(0.81, 0.81, 0.81, 0.5);
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2d((-0.9 + (stepSize * input.zDist.headMap(input.filterStart).size())), 0);
        gl.glVertex2d((-0.9 + (stepSize * input.zDist.headMap(input.filterStart).size())), 0.5);
        gl.glVertex2d((0.9 - (stepSize * input.zDist.tailMap(input.filterEnd).size())), 0.5);
        gl.glVertex2d((0.9 - (stepSize * input.zDist.tailMap(input.filterEnd).size())), 0);
        gl.glEnd();
        gl.glColor4d(1.0, 0.001, 0.001, 1);
        gl.glBegin(GL2.GL_TRIANGLES);
        gl.glVertex2d((-0.9 + (stepSize * input.zDist.headMap(input.filterStart).size()) - 0.05), 0.04);
        gl.glVertex2d((-0.9 + (stepSize * input.zDist.headMap(input.filterStart).size()) + 0.05), 0.04);
        gl.glVertex2d((-0.9 + (stepSize * input.zDist.headMap(input.filterStart).size())), 0.05);
        gl.glVertex2d((0.9 - (stepSize * input.zDist.tailMap(input.filterEnd).size()) - 0.05), 0.04);
        gl.glVertex2d((0.9 - (stepSize * input.zDist.tailMap(input.filterEnd).size()) + 0.05), 0.04);
        gl.glVertex2d((0.9 - (stepSize * input.zDist.tailMap(input.filterEnd).size())), 0.05);
        gl.glEnd();
        gl.glFlush();
		 xVal = -0.9;
		gl.glBegin(GL2.GL_QUAD_STRIP);
		for(int i=0; i<keyList.size(); i++){
			gl.glColor4d((double) getColor(keyList.get(i)).getRed() /255.0, (double) getColor(keyList.get(i)).getGreen() /255.0 , (double) getColor(keyList.get(i)).getBlue() /255.0, 1.0);
			gl.glVertex2d(xVal, 0.05);
			gl.glVertex2d(xVal, 0.09);
			xVal += stepSize;
		}
		gl.glEnd();
		gl.glFlush();
	}


	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2(); 
		glu = new GLU();
		glut = new GLUT();
		gl.glEnable(GL2.GL_POLYGON_SMOOTH);
		gl.glHint(GL2.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
		gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST);
		gl.glEnable(GL2.GL_POLYGON_SMOOTH_HINT);
		gl.glEnable(GL2.GL_LINE_SMOOTH_HINT);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POLYGON_SMOOTH);
		gl.glEnable(GL.GL_BLEND);//GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glClearColor(1, 1, 1, 0);
		gl.glLineWidth(0.9f);
		gl.glPointSize(2.0f);
	}


	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}
}
