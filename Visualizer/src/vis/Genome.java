package vis;

import java.awt.Color;
import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import static java.lang.Math.*;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;
//Eventually the other parsers should probably extend this. 
public abstract class Genome {


	public static final double BLOCK_WIDTH = 0.05;
    public static final double TICK_RADIUS = 1.02;
    public static final double LABEL_RADIUS = 1.05;
    public static final double FONT_SIZE_SML = 0.00013;
    public static final double FONT_SIZE_MED = 0.00015;
	Object templinks;
	//IDs of blocks mapped to their index in the array. 
	HashMap<String, Integer> blockIDs = new HashMap<String, Integer>();
	HashMap<String, Color> colorMap = new HashMap<String, Color>();
	//TODO Set it up so that each track is an array of blocks, tracks must extend Block 
	//class in future and override render and export methods. 
	//array of blocks 
	Block[] blocks = new Block[0];
	Node[][] nodes;
	//total number of base pairs. 
	int nbp = 0;
	double bpL;
	//block offset, same ID as in blockIDs. 
	int[] blockOffsets;
	int nLinks = 0; 
	//TODO set it up so that you can select multiple regions around the edge of the circle to keep as visible. Should the view be responsible for adding tracks? 
	//however, I do need some kind of data structure for keeping track of which regions are selected. 
	TreeMap<Double, Double> looseVisibleRegions = new TreeMap<Double, Double>();
	Double selectionStart;
	Double selectionEnd;
	
	//for z related things
	double zStart = Double.MIN_VALUE;
	double zEnd = Double.MAX_VALUE;
	TreeMap<Double, Integer> zDist = new TreeMap<Double, Integer>();
	double zModifier = 0.05;
	double filterStart = Double.MIN_VALUE;
	double filterEnd = Double.MAX_VALUE;
	

	double locEnd = 2*Math.PI;
	int maxZ = 0;
	
	abstract void setupInput(File fileName);
    int tickSpace = 1000;
    int minorTickSpace = 100;
    boolean minorTickLabels = false;
    boolean transparency = true;
    boolean gradient = true;

	abstract Block[] parseBlocks();
	abstract Stack<Link> parseLinks();
	
	
	
	Genome(File fileName){
		setupInput(fileName);
		colorSetup();
		blocks = parseBlocks();
		blockOffsets = new int[blocks.length];
		System.out.println("nBlocks = " + blocks.length);
		for (int i=0; i<blocks.length; i++){
			blocks[i].parent = this;
			blockOffsets[i] = blocks[i].blockOffset;
			blockIDs.put(blocks[i].ID, i);
		}
		bpL = (Math.PI * 2.0)/(double) nbp;
        tickSpace = nbp/10;
        minorTickSpace = nbp/100;
		Bundle.overlapT = (int) (0.1289 / Math.toDegrees(bpL));
		nbp += Bundle.overlapT * blocks.length;
		bpL = (Math.PI * 2.0)/(double) nbp;
        double circleDiameter = ((CircleView.WINDOW_HEIGHT - (CircleView.WINDOW_HEIGHT /11.0))* (1.0 -BLOCK_WIDTH));
        System.out.println("Circle diameter " + circleDiameter);

        double degreesPerPixel = (360.0/(2.0*circleDiameter));
        System.out.println("Deg per pixel: " + degreesPerPixel);
        Bundle.overlapT = (int) (degreesPerPixel / Math.toDegrees(bpL));
		Stack<Link> links = parseLinks();
		System.out.println("got " + links.size() + " links");
		//now here is a 2d array of treesets of links. 
		TreeSet<Link>[][] sortedLinks = (TreeSet<Link>[][]) Array.newInstance(TreeSet.class, new int[]{blocks.length, blocks.length});
		nodes = (Node[][]) Array.newInstance(Node.class, new int[]{blocks.length, blocks.length});
		for(int i=0; i<blocks.length; i++){
			blocks[i].totalBP(nbp);
			blocks[i].blockOffset+= Bundle.overlapT * i;
			blocks[i].start+= Bundle.overlapT * i;
			blocks[i].end+= Bundle.overlapT * i;
			blockOffsets[i] = blocks[i].blockOffset;
			for (int j=i; j<blocks.length; j++)
				sortedLinks[i][j] = new TreeSet<Link>();
		}
		System.out.println(sortedLinks[0][0].getClass().toString());
		while(!links.isEmpty()){
			Link l = links.pop();
			if(zDist.containsKey(l.z)){
				zDist.put(l.z, zDist.get(l.z) + 1);
				if (maxZ < zDist.get(l.z)) maxZ = zDist.get(l.z);
			}
			else zDist.put(l.z, 1);
			sortedLinks[Math.min(l.startBlock, l.endBlock)][Math.max(l.startBlock, l.endBlock)].add(l);
		}
		System.out.println("successfully put everything in the array?");
        int nBuckets = zDist.size();
		if(zDist.size()>1000)
            nBuckets = 1000;
        TreeMap<Double, Integer> placeHolder = new TreeMap<Double, Integer>();
        Double stepSize = (zDist.lastKey() - zDist.firstKey()) / nBuckets;
        Double key = zDist.firstKey();
        for(int i=0; i<nBuckets; i++){
            Integer value = 0;
            for(Double k : zDist.subMap(key, key + stepSize).keySet()){
                value += zDist.get(k);
            }
            placeHolder.put(key, value);
            key += stepSize;
        }
        zDist = placeHolder;
        int totalBundles = 0;
		//TODO fix this. 
		for(int i=0; i<blocks.length; i++){
			for (int j=i; j<blocks.length; j++) if(sortedLinks[i][j].size()>0){
				System.out.println(sortedLinks[i][j].size() + " in " +  i + "," + j);
				TreeSet<Bundle> bundledLinks = new TreeSet<Bundle>();
				Link l = sortedLinks[i][j].pollFirst();
                Bundle temp = new Bundle(l);
				bundledLinks.add(temp);
				while(sortedLinks[i][j].size()>0){
					l = sortedLinks[i][j].pollFirst();
                    temp = new Bundle(l);
					//System.out.println(l.toString());
					if(bundledLinks.ceiling(temp) != null && bundledLinks.ceiling(temp).compareTo(l)==0){
						bundledLinks.ceiling(temp).addLink(l);
						//System.out.println("bundled");
					}
					else if(bundledLinks.floor(temp) != null && bundledLinks.floor(temp).compareTo(l) == 0){
						bundledLinks.floor(temp).addLink(l);
					}
					else{
						bundledLinks.add(temp);
						//System.out.println("not bundled");
					}
				}
				System.out.println("bundled links size: " + bundledLinks.size());
                totalBundles += bundledLinks.size();
				//First add the middle bundle to the tree. 
				//TODO build a balanced tree.
				nodes[i][j] = new Node(bundledLinks.pollFirst(), null);
				//bundledLinks.remove(bundledLinks.size()/2);
				for(Bundle b : bundledLinks){
					nodes[i][j].addNode(b);
					//System.out.println(nodes[i][j].children);
				}
			}
		}
		System.out.println("Total bundles " + totalBundles);
		System.out.println(Bundle.overlapT + " overlap threshold");
		//addVisibleRegion(5223.0 * bpL, Math.PI * 2.0);
        System.out.println("biggest  bundle: " + Bundle.biggestBundle);
	}
	
	Genome(Genome g){
		//Need to : 
		//set up the new blocks 
		blocks = new Block[g.looseVisibleRegions.size()];
		blockOffsets = new int[blocks.length];
		int i=0;
		nbp = 0;
		for(Double key : g.looseVisibleRegions.keySet()){
			System.out.println("key " + (key / g.bpL));
			blocks[i] = new Block(g.blocks[g.getBlockByAngle(key)].ID + " " + key.toString(), (int) (key / g.bpL), (int) ((g.looseVisibleRegions.get(key) / g.bpL)), nbp);
			nbp += blocks[i].length;
			blocks[i].parent = this;
			blockOffsets[i] = blocks[i].blockOffset;
			blockIDs.put(blocks[i].ID + i, i);
			i++;
		}
		bpL = (Math.PI * 2.0)/(double) nbp;
		//Bundle.overlapT = (int) (0.1289 / Math.toDegrees(bpL));
		nbp += Bundle.overlapT * blocks.length;
		bpL = (Math.PI * 2.0)/(double) nbp;
		for(int j=0; j<blocks.length; j++){
			blocks[j].totalBP(nbp);
			blocks[j].blockOffset+= Bundle.overlapT * j;
			blocks[j].start+= Bundle.overlapT * j;
			blocks[j].end+= Bundle.overlapT * j;
			blockOffsets[j] = blocks[j].blockOffset + blocks[j].start;
			System.out.println("ID: " + blocks[j].ID + " start " + blocks[j].start + " end " + blocks[j].end + " offset " + blocks[j].blockOffset + " length " + blocks[j].length);
		}
		nodes = (Node[][]) Array.newInstance(Node.class, new int[]{blocks.length, blocks.length});
		//TODO this is now the biggest problem. 
		for(i=0; i<blocks.length; i++){
			for(int j=0; j<blocks.length; j++){
				int originalI =  g.blockIDs.get(blocks[i].ID.split("[ ]")[0]);
				int originalJ = g.blockIDs.get(blocks[j].ID.split("[ ]")[0]);
				
				if(originalI<=originalJ){
					if(g.nodes[originalI][originalJ]!=null){
						//System.out.println("GetLCA");
					nodes[i][j] = getLowestCommonAncestor(blocks[i].start, blocks[i].end, blocks[j].start, blocks[j].end, zStart, zEnd, g.nodes[originalI][originalJ]);
					}
				}
				else{ 
					if(g.nodes[originalJ][originalI]!=null){
						//System.out.println("GetLCA");
					nodes[i][j] = getLowestCommonAncestor(blocks[j].start, blocks[j].end, blocks[i].start, blocks[i].end, zStart, zEnd, g.nodes[originalJ][originalI]);
					}
				}
				if(nodes[i][j] != null) {
					//System.out.println("updating node: " + nodes[i][j]);
					nodes[i][j].updateChildren(this, i, j);

				}
			}
		}
		zDist = g.zDist;
	}
	
	void unFilter(){
		for (int i=0; i<blocks.length; i++)
			for (int j=i; j<blocks.length; j++){
				if(nodes[i][j]!=null) nodes[i][j].updateChildren(this, i, j);
			}
	}
	
	ArrayList<String> getBlockIDs(){
		return new ArrayList<String>(blockIDs.keySet());
	}
	
	int nLinks(){return nLinks;}
	
	int nBlocks(){return blocks.length;}
		
	int getBlockByAngle(double t){
		for (int i=0; i<blocks.length; i++){
			if(blocks[i].startT()<=t && blocks[i].endT()>=t) return i;
		}
		//System.out.println(blocks[blocks.length-1].endT() == Math.PI * 2.0);
		return -1;
	}
	
//	ArrayList<Block> blocks(){
//		ArrayList<Block> al = new ArrayList<Block>();
//		for (int i=0; i<blocks.length; i++){
//			al.add(blocks[i]);
//		}
//		return al;
//	}
	
	/**
	 * Methods relating to selecting regions within the genome. 
	 */
	//THESE TREES WERE A BAD IDEA. 
	void addVisibleRegion(double start, double end){
		//the start is always before the end. 
		if(end<start){
			double t = start;
			start = end;
			end = t;
		}		
		//if the only visible region is the full circle: 
		if(looseVisibleRegions.size()==1)
			if(looseVisibleRegions.get(looseVisibleRegions.firstKey()).equals(locEnd)) looseVisibleRegions = new TreeMap<Double, Double>();
		//add this region to the map. if it's the only one: 
			double newEnd = end;
			//the subset of regions beginning within this one: 
			//if any of them are contained within this region, remove them. this also removes this region if it was already present. 
			ArrayList<Double> removeKeys = new ArrayList<Double>();
			for(Double sKey : looseVisibleRegions.subMap(start, true, end, true).keySet()){
				if(looseVisibleRegions.subMap(start, true, end, true).get(sKey)<end) removeKeys.add(sKey);
			}
			for(Double d : removeKeys){
				looseVisibleRegions.remove(d);
			}
			//if ss is not now empty, the one remaining item is the region overlapping this one to the right. 
			if (looseVisibleRegions.subMap(start, true, end, true).size() == 1){
				newEnd = looseVisibleRegions.subMap(start, true, end, true).get(looseVisibleRegions.subMap(start, true, end, true).firstKey());
				looseVisibleRegions.subMap(start, true, end, true).clear();}
			//now SS is empty and also we deleted the entry overlapping this one to the right. 
			if(looseVisibleRegions.floorKey(start)!=null){
				//there is something to the left of this. 
				if(looseVisibleRegions.get(looseVisibleRegions.floorKey(start))>start){
					//it overlaps this one. 
					looseVisibleRegions.put(looseVisibleRegions.floorKey(start), newEnd);
				}
				else{
					looseVisibleRegions.put(start, newEnd);
				}
			}
			else{ //there is nothing left of this. 
				looseVisibleRegions.put(start, newEnd);
			}
	}
	
	void removeVisibleRegion(double start, double end){
		double pkey;
		double nend;
		if(!looseVisibleRegions.containsKey(start)){
			if(looseVisibleRegions.floorKey(start)!=null){
			pkey = looseVisibleRegions.floorKey(start);
			nend = looseVisibleRegions.remove(pkey);
			looseVisibleRegions.put(pkey, start);
			looseVisibleRegions.put(end, nend);
			}
		}
		else{
			nend = looseVisibleRegions.remove(start);
			if(nend!=end) looseVisibleRegions.put(end, nend);
		}
	}
	
	void startSelection(double t){
		selectionStart = t;
		selectionEnd = t;
	}
	
	void moveSelection(double t){
		if(selectionStart == null) selectionStart = t;
		else if (t>selectionStart)
			selectionEnd = t;
		else selectionStart = t;
	}
	
	void finishSelection(double t){
		if(selectionStart!=null) addVisibleRegion(selectionStart, selectionEnd);
		selectionStart = null;
		selectionEnd = null;
	}
	
	/**
	 * Misc
	 */
	void colorSetup(){
		colorMap.put("red", new Color(255, 0, 0));
		colorMap.put("blue", new Color(0,0, 255));
		colorMap.put("green", new Color(0, 255, 0));
	}
	
	int nBP(){
		return nbp;
	}
	public double bpL() {
		return bpL;
	}

	public void render(GL2 gl, GLUT glut) {
		for (Block b : blocks){
			//System.out.println("rendering block " + b.ID);
			b.render(gl, glut);
            renderLabel(gl, glut, b.ID, b.startT() + 0.5* (b.endT() - b.startT()), LABEL_RADIUS, FONT_SIZE_SML);
            //((double) i * bpL) - 0.005, TICK_RADIUS, FONT_SIZE_SML
		}
        System.out.println("rendered blocks");
		if(looseVisibleRegions.size()==0){
			for( int i=0; i< blocks.length; i++){
				for (int j=i; j<blocks.length; j++){
					//render strict query for the length of these whole blocks.
					if(nodes[i][j] != null){
						//System.out.println("filter start "+ filterStart);
						renderRange(blocks[i].start, blocks[i].end, blocks[j].start, blocks[j].end, filterStart, filterEnd, nodes[i][j], gl);
						//System.out.println("render region " + i + " " + j);
					}
				}
			}
		}
		else{
			for (Double regionStart : looseVisibleRegions.keySet()){
				int startBlock = getBlockByAngle(regionStart);
				int start = (int) ((regionStart) / bpL);
				int end = (int) ((looseVisibleRegions.get(regionStart)) / bpL);
				for(int endBlock = 0; endBlock<blocks.length; endBlock++){
					if(startBlock == endBlock && nodes[startBlock][startBlock] != null){
						//System.out.println("startblock == endblock");
						//System.out.println( " start " + start );
						//render all links starting within the region. 
						renderRange(start, end, blocks[startBlock].start, blocks[startBlock].end, filterStart, filterEnd, getLowestCommonAncestor(start, end, 0, blocks[startBlock].end, -1.0f, 1.0f, nodes[startBlock][startBlock]), gl);
						//render all the links ending within the region and starting "west" of it. 
						renderRange(blocks[startBlock].start, start, start, end, filterStart, filterEnd, nodes[startBlock][startBlock], gl);
						//render all the links ending within the region and starting "east" of it. 
						renderRange(end, blocks[startBlock].end, start, end, filterStart, filterEnd, nodes[startBlock][startBlock], gl);
					}
					else if(startBlock<endBlock && nodes[startBlock][endBlock] != null){
						renderRange(start, end, blocks[endBlock].start, blocks[endBlock].end, filterStart, filterEnd, getLowestCommonAncestor(start, end, 0, blocks[endBlock].end, -1.0f, 1.0f, nodes[startBlock][endBlock]), gl);
					}
					else if (nodes[endBlock][startBlock]!= null){
						renderRange(blocks[endBlock].start, blocks[endBlock].end, start, end, filterStart, filterEnd, nodes[endBlock][startBlock], gl);
					}
				}
			}
		}
        System.out.println("rendered links");
        for (Block b : blocks){
            //System.out.println("rendering block " + b.ID);
            b.render(gl, glut);
        }
        for (Double regionStart : looseVisibleRegions.keySet()){
            double s = regionStart;
            double e = looseVisibleRegions.get(s);
            renderRegion(gl, glut, s, e);
        }
		if(selectionStart != null){
			if (selectionEnd == null) selectionEnd = selectionStart;
			renderRegion(gl, glut, selectionStart, selectionEnd);
		}
        for(int i=0; i<nbp; i += minorTickSpace){
            if(i%tickSpace == 0){
            renderTick(gl, (double) i * bpL,  TICK_RADIUS - 0.003);
            renderLabel(gl, glut, Integer.toString(i), ((double) i * bpL) - 0.005, TICK_RADIUS, FONT_SIZE_MED);
            }
            else{

                renderTick(gl, (double) i * bpL,  TICK_RADIUS - 0.01);
                if(minorTickLabels) renderLabel(gl, glut, Integer.toString(i),((double) i * bpL) - 0.005, TICK_RADIUS, FONT_SIZE_SML );
            }
            gl.glFlush();
        }

	}

    public void renderTick(GL2 gl, double theta, double size){
        gl.glColor4d(0, 0, 0, 1);
        gl.glBegin(GL2.GL_LINE_LOOP);
        double[] point = Block.getCirclePoint(theta, size);
        gl.glVertex2d(point[0], point[1]);
        point = Block.getCirclePoint(theta, 1.0);
        gl.glVertex2d(point[0], point[1]);
        gl.glEnd();
    }

    public void renderLabel(GL2 gl, GLUT glut, String string, double theta, double radius, double size){
       gl.glPushMatrix();
        double extraSize;
       if(size == FONT_SIZE_MED)      {
            extraSize = 0.075;
            gl.glColor4d(0, 0, 0, 1);
       }
       else{
       gl.glColor4d(0.33, 0.33, 0.33, 1);
           extraSize = 0.04;
       }
       double degree = toDegrees(theta);
        if (degree<180){
            double[] point = Block.getCirclePoint(theta, radius);
            gl.glTranslated(point[0], point[1], 0.0);
            gl.glScaled(size, size, size);
            gl.glRotated(-degree + 90, 0, 0, 1);
        }
        else{
            double[] point = Block.getCirclePoint(theta, radius + extraSize);
            gl.glTranslated(point[0], point[1], 0.0);
            gl.glScaled(size, size, size);
            gl.glRotated(-degree - 90, 0, 0, 1);
        }
        glut.glutStrokeString(GLUT.STROKE_MONO_ROMAN, string);
        gl.glPopMatrix();
    }

    public void renderRegion(GL2 gl, GLUT glut, double start, double end){
        //render the markers.

        double[] point = Block.getCirclePoint(start);
        gl.glColor4d(1, 0, 0, 1);
        //the triangular red things
        gl.glBegin(GL2.GL_TRIANGLES);
        gl.glVertex2d(point[0], point[1]);
        point = Block.getCirclePoint(start - 0.012);
        gl.glVertex2d(point[4], point[5]);
        point = Block.getCirclePoint(start + 0.012);
        gl.glVertex2d(point[4], point[5]);
        point = Block.getCirclePoint(end);
        gl.glVertex2d(point[0], point[1]);
        point = Block.getCirclePoint(end - 0.012);
        gl.glVertex2d(point[4], point[5]);
        point = Block.getCirclePoint(end + 0.012);
        gl.glVertex2d(point[4], point[5]);
        gl.glEnd();

        renderLabel(gl, glut, Integer.toString((int) (start / bpL) - blocks[getBlockByAngle(start)].blockOffset), start, TICK_RADIUS, FONT_SIZE_MED);
        renderLabel(gl, glut, Integer.toString((int) (end / bpL) - blocks[getBlockByAngle(end)].blockOffset), end, TICK_RADIUS, FONT_SIZE_MED);

        gl.glColor4d(1, 1, 1, 0.45);
        //translucent space between the markers.
        gl.glBegin(GL2.GL_QUAD_STRIP);
        point = Block.getCirclePoint(start);
        gl.glVertex2d(point[0], point[1]);
        gl.glVertex2d(point[2], point[3]);
        for(Double key : Block.circlePoints.subMap(start,false, end, false).keySet()){
            double[] innerPoint = Block.circlePoints.get(key);
            gl.glVertex2d(innerPoint[0], innerPoint[1]);
            gl.glVertex2d(innerPoint[2], innerPoint[3]);
        }
        point = Block.getCirclePoint(end);
        gl.glVertex2d(point[0], point[1]);
        gl.glVertex2d(point[2], point[3]);
        gl.glEnd();
        gl.glFlush();
    }
    //TODO make this not traverse the entire tree every time.
    public void renderRange(int W, int E, int S, int N, double B, double F, Node n, GL2 gl){
        if (n.inRegion(W, E, S, N, B, F))
            n.bundle.render(gl);
            if (n.left!=null) renderRange(W, E, S, N, B, F, n.left, gl);
            if (n.right!= null) renderRange(W, E, S, N, B, F, n.right, gl);
    }

    public void  circosExportRange(PrintWriter out, int W, int E, int S, int N, double B, double F, Node n){
        if (n.inRegion(W, E, S, N, B, F)){ out.println(n.bundle.toCircosString());
        for (Link l : n.bundle.links){
            System.out.println("Link start1 " + l.start1 + " start2 " + l.start2 + " end1 " + l.end1 + " end2 "+ l.end2);
        }
        }
        if (n.left!=null) circosExportRange(out, W, E, S, N, B, F, n.left);
        if (n.right!= null) circosExportRange(out, W, E, S, N, B, F, n.right);
    }

    public void circosExport(PrintWriter out) {
        if(looseVisibleRegions.size()==0){
            for( int i=0; i< blocks.length; i++){
                for (int j=i; j<blocks.length; j++){
                    if(nodes[i][j] != null){
                        circosExportRange(out, blocks[i].start - blocks[i].blockOffset, blocks[i].end - blocks[i].blockOffset, blocks[j].start - blocks[j].blockOffset, blocks[j].end - blocks[j].blockOffset, filterStart, filterEnd, nodes[i][j]);
                    }
                }
            }
        }
        else{
            for (Double regionStart : looseVisibleRegions.keySet()){
                int startBlock = getBlockByAngle(regionStart);
                int start = (int) ((regionStart) / bpL) - blockOffsets[startBlock];
                int end = (int) ((looseVisibleRegions.get(regionStart)) / bpL) - blockOffsets[startBlock];
                for(int endBlock = 0; endBlock<blocks.length; endBlock++){
                    if(startBlock == endBlock && nodes[startBlock][startBlock] != null){
                        circosExportRange(out, start, end, blocks[startBlock].start, blocks[startBlock].end, filterStart, filterEnd, getLowestCommonAncestor(start, end, 0, blocks[startBlock].end, -1.0f, 1.0f, nodes[startBlock][startBlock]));
                        //render all the links ending within the region and starting "west" of it.
                        circosExportRange(out, blocks[startBlock].start, start, start, end, filterStart, filterEnd, nodes[startBlock][startBlock]);
                        //render all the links ending within the region and starting "east" of it.
                        circosExportRange(out, end, blocks[startBlock].end, start, end, filterStart, filterEnd, nodes[startBlock][startBlock]);
                    }
                    else if(startBlock<endBlock && nodes[startBlock][endBlock] != null){
                        circosExportRange(out, start, end, blocks[endBlock].start, blocks[endBlock].end, filterStart, filterEnd, getLowestCommonAncestor(start, end, 0, blocks[endBlock].end, -1.0f, 1.0f, nodes[startBlock][endBlock]));
                    }
                    else if (nodes[endBlock][startBlock]!= null){
                        circosExportRange(out, blocks[endBlock].start, blocks[endBlock].end, start, end, filterStart, filterEnd, nodes[endBlock][startBlock]);
                    }
                }
            }

        }
    }


//	public void export(PrintWriter out, int width){
//		for (Double regionStart : visibleRegions.keySet()){
//			int startBlock = getBlockByAngle(regionStart);
//			int endBlock = getBlockByAngle(visibleRegions.get(regionStart));
//			for(int i=startBlock; i<=endBlock; i++){
//				double s = regionStart; 
//				double e = visibleRegions.get(s);
//				blocks[i].exportRegion(out, s, e, width);
//			}
//		}
//		for(Block b : blocks){
//			b.export(out, width);
//		}
//	}
//	
	
	
	
	//TODO modify to work with selected regions. 
	/**
	 * Types of queries: 
	 * 1 dimensional, 2 dimensional, 3 dimensional. 
	 * the loose query is a type of 1d query. 
	 * For a 1d query, run twice on the tree containing links within one block. 
	 */
	//E/W = start 
    //N/S = end
	//F/B = z
	
	public Node getLowestCommonAncestor(int W, int E, int S, int N, double B, double F, Node root){
        return root;
	}

    public Node getNearestNeighbour(int start, int end, double z, Node r){
        //traverse the tree until you get to a leaf.
        Node currentNode = getNearestLeaf(start, end, z, r);
        //currentNode is the current best node now.
        Node currentBest = currentNode;
        double bestDistance = getDistance(start, end, z, currentBest);
        //Walk back to the top of the tree.
        while (currentNode!=r){
             if (getDistance(start, end, z, currentNode) < bestDistance){
                 bestDistance = getDistance(start, end, z, currentNode);
                 currentBest = currentNode;
             }
            // see whether the difference between the splitting coordinate of the search point and current node is less than the distance (overall coordinates) from the search point to the current best.
            if(currentNode.depth%3 ==0){
                 if(abs(currentNode.bundle.start1 - start)<getDistance(start, end, z, currentBest)){
                     if (start < currentNode.bundle.start1 && currentNode.left != null)
                         currentNode = getNearestLeaf(start, end, z, currentNode.left);
                     else if (currentNode.right != null)
                         currentNode = getNearestLeaf(start, end, z, currentNode.right);
                 }
                 else currentNode = currentNode.parent;
            }
            else if(currentNode.depth%3 == 1){
                if(abs(currentNode.bundle.end1 - end)<getDistance(start, end, z, currentBest)){
                    if (end < currentNode.bundle.end1 && currentNode.left != null)
                        currentNode = getNearestLeaf(start, end, z, currentNode.left);
                    else if (currentNode.right != null)
                        currentNode = getNearestLeaf(start, end, z, currentNode.right);
                }
                else currentNode = currentNode.parent;
            }
            else if(currentNode.depth%3 == 2){
                if(abs(currentNode.bundle.z - z)<getDistance(start, end, z, currentBest)){
                    if (z < currentNode.bundle.z && currentNode.left != null)
                        currentNode = getNearestLeaf(start, end, z, currentNode.left);
                    else if (currentNode.right != null)
                        currentNode = getNearestLeaf(start, end, z, currentNode.right);
                }
                else currentNode = currentNode.parent;
            }
        }
        return currentBest;
    }

    private Node getNearestLeaf(int start, int end, double z, Node currentNode){
        while (currentNode.left != null || currentNode.right != null){
            if (currentNode.depth%3 == 0){
                if (start<currentNode.bundle.start1){
                    if (currentNode.left != null) currentNode = currentNode.left;
                    else currentNode = currentNode.right;
                }
                else{
                    if (currentNode.right != null) currentNode = currentNode.right;
                    else currentNode = currentNode.left;
                }
            }
            else if (currentNode.depth%3 == 1){
                if (end < currentNode.bundle.end1){
                    if (currentNode.left != null) currentNode = currentNode.left;
                    else currentNode = currentNode.right;
                }
                else{
                    if (currentNode.right != null) currentNode = currentNode.right;
                    else currentNode = currentNode.left;
                }
            }
            else if (currentNode.depth%3 == 2){
                if (z < currentNode.bundle.z){
                    if (currentNode.left != null) currentNode = currentNode.left;
                    else currentNode = currentNode.right;
                }
                else{
                    if (currentNode.right != null) currentNode = currentNode.right;
                    else currentNode = currentNode.left;
                }
            }
        }
        return currentNode;
    }

    private double getDistance(int start, int end, double z, Node n){
        return Math.pow((n.bundle.start1 - start), 2) + Math.pow((n.bundle.end1 - end), 2) + Math.pow((n.bundle.z - z), 2);
    }
	
	private class Node{
		Bundle bundle;
		int depth;
        Node parent;
		Node left;
        Node right;
		
		Node(Bundle b, Node p){
			bundle = b;
			parent = p;
            if (parent==null)  depth = 0;
            else depth = parent.depth + 1;
		}
        //splitting planes:
        //0:start
        //1:end
        //2:z
		public void addNode(Bundle b){
            if (depth%3 ==0){
                if (b.start1<=bundle.start1){
                    if (left == null) left = new Node(b, this);
                    else left.addNode(b);
                }
                else{
                    if (right == null) right = new Node(b, this);
                    else right.addNode(b);
                }
            }
            else if(depth%3 == 1){
                if(b.end1<=bundle.end1){
                    if (left == null) left = new Node(b, this);
                    else left.addNode(b);
                }
                else{
                    if (right == null) right = new Node(b, this);
                    else right.addNode(b);
                }

            }
            else if(depth%3 ==2){
                if(b.z<=bundle.z){
                    if (left == null) left = new Node(b, this);
                    else left.addNode(b);
                }
                else{
                    if (right == null) right = new Node(b, this);
                    else right.addNode(b);
                }

            }
		}
		
		//Returns true if the node is strictly within the region defined (both ends of both ends).
		public boolean inRegion(int W, int E, int S, int N, double B, double F){
			//System.out.println("W " + W + " E " + E + " N " + N + " S " + S + " bundle.start1 " + bundle.start1);
			if (((bundle.start1 + bundle.parent.blocks[bundle.startBlock].blockOffset>=W && bundle.start1+ bundle.parent.blocks[bundle.startBlock].blockOffset<E)
					&&(bundle.start2+ bundle.parent.blocks[bundle.startBlock].blockOffset>=W && bundle.start2+ bundle.parent.blocks[bundle.startBlock].blockOffset<E))&&
					((bundle.end1+ bundle.parent.blocks[bundle.endBlock].blockOffset>=S&&bundle.end1 + bundle.parent.blocks[bundle.endBlock].blockOffset<N)
							&&(bundle.end2 + bundle.parent.blocks[bundle.endBlock].blockOffset>=S&&bundle.end2 + bundle.parent.blocks[bundle.endBlock].blockOffset<N)&&
					(bundle.z>=B && bundle.z<=F))
					) return true;
			else return false; 
		}
		
		public void updateChildren(Genome newParent, int sb, int eb){
        //TODO fix this
		}
	}
}
