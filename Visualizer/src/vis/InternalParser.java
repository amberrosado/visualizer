package vis;

import java.io.File;
import java.util.Stack;
import java.util.TreeSet;

public class InternalParser extends Genome {
	
InternalParser(Genome original) {
		super(original);
	}

	@Override
	void setupInput(File fileName) {
		
	}

	@Override
	//need to figure out the set of start and end blocks visible.
	Block[] parseBlocks() {
		return null;
	}

	@Override
	Stack<Link> parseLinks() {
		return null;
	}

}
