package vis;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public class MizBeeParser extends Genome {
	BufferedReader conf;
	ArrayList<String> tempblocks = new ArrayList<String>();
	ArrayList<String> templinks;
	
	MizBeeParser(File fileName) {
		super(fileName);
	}

	public static void main(String[] args) throws IOException{
		System.out.println(System.currentTimeMillis());
		System.out.println("we did it?");
		System.out.println(System.currentTimeMillis());
	}
    private Color getColor(int n){
        switch (n%22){
            case 0:     return new Color(166, 206, 227);
            case 1:     return new Color(31, 120, 180);
            case 2:     return new Color(178, 223, 138);
            case 3:     return new Color(51, 160, 44);
            case 4:     return new Color(251, 154, 153);
            case 5:     return new Color(227, 26, 28);
            case 6:     return new Color(253, 191, 111);
            case 7:     return new Color(255, 127, 0);
            case 8:     return new Color(202, 178, 214);
            case 9:     return new Color(106, 61, 154);
            case 10:    return new Color(255, 255, 153);
            case 11:    return new Color(83, 103, 114);
            case 12:    return new Color(15, 60, 90);
            case 13:    return new Color(89, 112, 69);
            case 14:    return new Color(26, 80, 22);
            case 15:    return new Color(125, 77, 76);
            case 16:    return new Color(114, 13, 14);
            case 17:    return new Color(126, 63, 55);
            case 18:    return new Color(127, 66, 0);
            case 19:    return new Color(101, 89, 107);
            case 20:    return new Color(53, 30, 82);
            case 21:    return new Color(127, 127, 77);
        }
        return new Color(0, 0, 0);
    }

	@Override
	void setupInput(File fileName) {
		tempblocks = new ArrayList<String>();
		templinks = new ArrayList<String>();
		try{
			conf = new BufferedReader(new FileReader((File) fileName));
            String path = fileName.getPath();
            String name = fileName.getName();
            int index = path.indexOf(name);
            String ss = path.substring(0, index);
			String line = conf.readLine();
            System.out.println(line);
			conf.close();
			BufferedReader in = new BufferedReader(new FileReader(ss + line.trim()));
            System.out.println("opened fish");
			line = in.readLine();
            System.out.println(line);
			String[] arr = line.split("\t");
            int nGenomes = 0;
			while (arr[0].equals("genome")){
                nGenomes++;
				line = in.readLine();
				arr = line.split("\t");
				int nchrom = Integer.parseInt(arr[1]);
				for (int i=0; i<nchrom; i++){
					tempblocks.add(in.readLine() + "\t" + nGenomes);
				}
				in.readLine();
				line = in.readLine();
				arr = line.split("\t");
			}
			templinks.add(line);
			while(in.ready()) templinks.add(in.readLine());
			in.close();
		} catch(Exception e){
			System.out.println("setup input exception");
			System.out.println(tempblocks);
			System.out.println(templinks);
		}
	}

	@Override
	Block[] parseBlocks() {
		Block[] returned = new Block[tempblocks.size()];
		for (int i=0; i<tempblocks.size(); i++){
			String[] arr = tempblocks.get(i).split("\t");
			returned[i] = new Block(arr[0], nbp, Integer.parseInt(arr[1]) + nbp, nbp);
            if(Integer.parseInt(arr[2]) == 1)
                returned[i].setColor(getColor(i));
            else
                returned[i].setColor(new Color(1, 102, 94));
			nbp += Integer.parseInt(arr[1]);
		}
		return returned;
	}

	@Override
	Stack<Link> parseLinks() {
		String[] arr;
		Stack<Link> stack = new Stack<Link>();
		try{
		for(int i=0; i<templinks.size(); i++){
			while(templinks.get(i).equals("")) i++;
			arr = templinks.get(i).split("\t");
			int startBlock = blockIDs.get(arr[0]);
			int endBlock = blockIDs.get(arr[4]);
			int linksInGroup = Integer.parseInt(arr[9]);
			for(int j=0; j<linksInGroup; j++){
				i++;
				arr = templinks.get(i).split("\t");
				stack.add(new Link(startBlock, Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), endBlock, Integer.parseInt(arr[3]), Integer.parseInt(arr[4]), Float.parseFloat(arr[6]), this));
			}
		}
		}catch(Exception e){
		}
		return stack;
	}
}

