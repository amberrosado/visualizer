package vis;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import static java.lang.Math.*;

public class Link implements Comparable<Link>{
	   static int splits = 12;
	   int start1;
	   int start2;
	   int end1;
	   int end2;
	   int startBlock;
	   int endBlock;
	   Genome parent;
	   double z;
       boolean orientation;

	   
	   //Static methods: Calculate points on a bezier curve. 
		public static double bx(double d, double[] cp){
			return (pow(1.0-d, 2.0) * cp[0]) + (2*(1.0-d)*d*cp[3]) + (pow(d, 2)*cp[6]);
		}
		public static double by(double d, double[] cp){
			return (pow(1.0-d, 2.0) * cp[1]) + (2*(1.0-d)*d*cp[4]) + (pow(d, 2)*cp[7]);
		}
		public static double bz(double d, double[] cp){
			return (pow(1.0-d, 2.0) * cp[2]) + (2*(1.0-d)*d*cp[5]) + (pow(d, 2)*cp[8]);
		}

        /*
          startBlock is always less than endBlock.
          start1 is always less than start2
          if end1<end2 orientation==true, else orientation==false
         */

	   Link(int sb, int s1, int s2, int eb, int e1, int e2, double option, Genome p){
           if(sb<=eb){
               startBlock = sb;
               endBlock = eb;
               if(s1<s2){
                    start1 = s1;
                    start2 = s2;
                    end1 = e1;
                    end2 = e2;
               }
               else{
                   start1 = s2;
                   start2 = s1;
                   end1 = e2;
                   end2 = e1;
               }
           }
           else{
               startBlock = eb;
               endBlock = sb;
               if(e1<e2){
                   start1 = e1;
                   start2 = e2;
                   end1 = s1;
                   end2 = s2;
               }
               else{
                   start1 = e2;
                   start2 = e1;
                   end1 = s2;
                   end2 = s1;
               }
           }
           if (end1<end2)   orientation = true;
           else orientation = false;
           z = option;
           parent = p;
	   }
	   
	   public double z(){
		   return z;
	   }
	   
	   public int start(){
		   return start1;
	   }
	   
	   public int end(){
		   return end1;
	   }


	@Override
	//Two links are equal if their start, startblock, endblock, end, length and z values are all equal. 
	//They are sorted by start block, start, endblock, end, z value and then length. 
	//TODO this will need to be modified also.
	public int compareTo(Link other) {
		if(this.startBlock<other.startBlock) return -1;
		else if(this.startBlock>other.startBlock) return 1;
		else{
			if(this.endBlock<other.endBlock) return -1;
			else if(this.endBlock>other.endBlock) return 1;
			else{
				if(this.start1<other.start1) return -1;
				else if(this.start1>other.start1) return 1;
				else{
					if(this.start2<other.start2) return -1;
					else if(this.start2>other.start2) return 1;
					else{
						if(this.end1<other.end1) return -1;
						else if(this.end1>other.end1) return 1;
						else{
							if(this.end2<other.end2) return -1;
							else if(this.end2>other.end2) return 1;
							else{
								if(this.z<other.z) return -1;
								else if(this.z>other.z) return 1;
								else return 0;
							}
						}
					}
				}
			}
		}
    }

	public String toString(){ return "Link: sb: " + startBlock + " start1, start2: " + start1 + "," + start2 + 
			" end1, end2: " + end1 + "," + end2 + " eb: " + endBlock;
	}
}

