package vis;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.jogamp.newt.Window;

public class InteractiveMediator implements MouseListener, ActionListener, KeyListener, MouseMotionListener {
	GLJPanel window;
	GLJPanel window2;
	CircleView view;
	ControlWindow controls;
	Genome input;
	Frame frame;
	Container buttonBox;
	static MouseEvent pressed;
	boolean dragging;
	int block; 
	//Block popup menu items
	PopupMenu blockMenu = new PopupMenu("menu");
	JColorChooser colorChooser = new JColorChooser();
	JDialog chooser;
	boolean colorPicker;
	TextField top;
	TextField bottom;
    TextField ticks;
    TextField minorTicks;
    TextField maxOpacity;
    Checkbox minorTickLabels;
    Checkbox transparency;
    Checkbox gradient;
    Container zBox;
    ArrayList<Button> buttons = new ArrayList<Button>();
    Menu fileMenu;
    FileDialog openFileDialog;
	//Colour picker? 
	
	//file dialog for exporting
	  //File chooser. 
	  FileDialog fileDialog;
	
	public InteractiveMediator(Frame f, CircleView circleView, GLJPanel w){
		colorChooser.setPreviewPanel(new JPanel());
		frame = f;
		blockMenu.add(new MenuItem("Set Colour"));
		blockMenu.add(new MenuItem("Remove Selection"));
		frame.add(blockMenu);
		blockMenu.addActionListener(this);
		chooser = JColorChooser.createDialog(frame,
                "Choose a color",
                true,
                colorChooser,
                this,
                this);
		view = circleView;
		window = w;
        window.addMouseListener(this);
        window.addMouseMotionListener(this);
		input = CircleView.visibleData;
        openFileDialog = new FileDialog(frame);
		fileDialog = new FileDialog(frame);
		fileDialog.setMode(FileDialog.SAVE);
		fileDialog.setTitle("Export to...");
	}
	public void giveControls(ControlWindow v2, GLJPanel w2){
		controls = v2;
		window2 = w2;
        window2.addMouseListener(this);
	}

    public void giveButtons(Container box){
        buttonBox = box;
        zBox = new Container();
        zBox.setLayout(new GridLayout(9, 2));
        top = new TextField("0", 5);
        bottom = new TextField("1", 5);
        zBox.add(new Label("Minimum weight of links displayed"));
        zBox.add(top);
        zBox.add(new Label("Maximum weight of links displayed"));
        zBox.add(bottom);
        zBox.add(new Label("Large ticks every:"));
        ticks = new TextField("1000");
        zBox.add(ticks);
        minorTicks = new TextField("100");
        zBox.add(new Label("Small ticks every:"));
        zBox.add(minorTicks);
        zBox.add(new Label("Print labels on small ticks?"));
        minorTickLabels = new Checkbox("", false);
        zBox.add(minorTickLabels);
        transparency = new Checkbox("", true);
        zBox.add(new Label("Use link weight for opacity"));
        zBox.add(transparency);
        gradient = new Checkbox("", true);
        zBox.add(new Label("Use link weight for colour"));
        zBox.add(gradient);
        maxOpacity = new TextField("0.5");
        zBox.add(new Label("Maximum link opacity"));
        zBox.add(maxOpacity);
        zBox.add(new Label(" "));
        Button apply = new Button("Apply settings");
        zBox.add(apply);
        buttons.add(new Button(" "));
        buttons.add(new Button(" "));
        apply.addActionListener(this);
        buttonBox.setLayout(new  GridLayout(buttons.size()+1, 1));
        buttonBox.add(zBox);

    }

    public void init(){
        //set up so that initially the best 500 links are displayed?
        Double bestZ = input.zDist.lastKey();
        int nLinks = input.zDist.get(bestZ);
        while(nLinks < 500){
            bestZ = input.zDist.lowerKey(bestZ);
            nLinks += input.zDist.get(bestZ);
        }
        top.setText(input.zDist.firstKey().toString());
        bottom.setText(input.zDist.lastKey().toString());
        ticks.setText(Integer.toString(input.tickSpace));
        minorTicks.setText(Integer.toString(input.minorTickSpace));
        input.filterStart = bestZ;
        top.setText(bestZ.toString());
        input.filterEnd = input.zDist.lastKey();
        fileMenu.add(new MenuItem("Export png"));
        fileMenu.add(new MenuItem("Export to circos links file"));
    }

	@Override
	//Modify so that a selection marker appears where clicked, moves with dragging, etc. 
	public void mouseClicked(MouseEvent e) {
	System.out.println(e.getButton() + " clicked");	
	block=overBlock(e.getX(), e.getY());
    if (e.getButton() == 3){
    		pressed = e;
			blockMenu.show(frame, e.getX(), e.getY());
	}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		dragging = true;
		input.moveSelection(polT(e.getX(), e.getY()));
		window.display();
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println(e.getSource() == frame);
	}
//
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("pressed");
		if(overBlock(e.getX(), e.getY())>-1 && e.getButton()==1){
			input.startSelection(polT(e.getX(), e.getY()));
		}
		window.display();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
        System.out.println("released");
		if(dragging){
		input.finishSelection(polT(e.getX(), e.getY()));
		window.display();
		}
	}

//	@Override
//	public void mouseWheelMoved(MouseEvent e) {
//		// TODO Auto-generated method stub
//		//System.out.println(e.toString());
//	}

	//returns true if these x,y co-ordinates (as given by the mouse event) are or might be over a block.
	private int overBlock(int x, int y){
		double t = polT(x, y);
		double r = polR(x, y);
		System.out.println(r);
		if(r<=1 && r>=1-Genome.BLOCK_WIDTH){
			return input.getBlockByAngle(t);
		}
		else return -1;
	}
	
	//The polar co-ordinates should only apply to the left third of the window. 
	private double polT(int x, int y){
		double cx = (x - (window.getHeight()/2.0))/(window.getHeight()/2.0);
		double cy = -(y - (window.getHeight()/2.0))/(window.getHeight()/2.0);
		double t = Math.atan2(cx, cy);
		if (t<0) t = (2* Math.PI) + t;
		return t;
	}
	
	private double polR(int x, int y){
		double cx = (x - (window.getHeight()/2.0))/(window.getHeight()/2.2);
		double cy = -(y - (window.getHeight()/2.0))/(window.getHeight()/2.2);
		double t = Math.atan2(cx, cy);
		return cx/Math.sin(t);
	}

    @Override
    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar()=='x'){
			//view.export(Long.toString(e.getWhen()));
		}
		if (e.getKeyChar()=='u'){
			window.display();
		}
		if (e.getKeyChar()=='f'){
			//view.filterByGoodness(0.8f, 1.0f);
			window.display();
		}
		if (e.getKeyChar()=='r'){
			view.filterData();
			input = view.visibleData;
			window.display();
		}
		if (e.getKeyChar()=='s'){
			input = view.visibleData;
			window.display();
		}
		
		if(e.getKeyChar()=='-'){
			input.zModifier -= 0.0002;
			window.display();
		}
		if(e.getKeyChar()=='+'){
			input.zModifier += 0.0002;
			window.display();
		}
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println(arg0.getSource());
		System.out.println(arg0.getActionCommand());
		switch (arg0.getActionCommand()){
		case "Set Colour": {
			colorPicker = true;
			chooser.setVisible(true);
			break;
		}
		case "Apply settings":{
			input.filterStart = Double.parseDouble(top.getText());
			input.filterEnd = Double.parseDouble(bottom.getText());
            input.tickSpace = Integer.parseInt(ticks.getText());
            input.minorTickSpace = Integer.parseInt(minorTicks.getText());
            input.minorTickLabels = minorTickLabels.getState();
            input.transparency = transparency.getState();
            input.gradient = gradient.getState();
            Bundle.MAX_OPACITY = Double.parseDouble(maxOpacity.getText());
			window.display();
            controls.display(controls.window);
			break; 
		}
		case "Remove Selection":{
			Double start = polT(pressed.getX(), pressed.getY());
			start = input.looseVisibleRegions.floorKey(start);
			if (start!=null){
				if(input.looseVisibleRegions.get(start)>=polT(pressed.getX(), pressed.getY()))
					input.removeVisibleRegion(start, input.looseVisibleRegions.get(start));
			}
			pressed = null;
			window.display();
			break;
		}

            case "Export png":{
                BufferedImage image = new BufferedImage(window.getWidth(), window.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g = image.createGraphics();
                window.printAll(g);
                image.flush();
                try {
                    ImageIO.write(image, "png", new File("image.png"));
                } catch (IOException ex){}
                break;
            }
		
		case "OK":{
			if (colorPicker){
				colorPicker = false;
				input.blocks[block].setColor(colorChooser.getColor());
				window.display();
				break;
			}
		}
		case "Export to circos links file":{
			fileDialog.setFile("*.svg");
			fileDialog.setVisible(true);
			view.circosExport(fileDialog.getFile());
			break;
		}
		case "Filter selected":{
			view.filterData();
			input = CircleView.visibleData;
			window.display();
			break;
		}
		case "Remove filter":{
			view.unFilter();
			input = CircleView.visibleData;
			window.display();
			break;
		}
		}
		
	}

}
