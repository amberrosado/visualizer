package vis;


import java.awt.Color;
import java.io.PrintWriter;
import java.util.TreeSet;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import static java.lang.Math.*;


public class Bundle extends Link{
	static int bundledLinks = 0;
	static int biggestBundle = 0;
    static double MAX_OPACITY = 0.5;

	TreeSet<Link> links;
	TreeSet<Bundle> bundles = null;
	float totalZ = 0; 
	double[][] control = new double[2][9];
	double[][] theta = new double[2][3]; 
	static double zThreshold = 0.1;
	static int overlapT = 0;
	int originalOverlap;
	double chordD;
	double linkLength;
	double controlDistance;
	double bpLength;
	
	
	Bundle(int sb, int start1, int start2, int eb, int end1, int end2,
			double option, Genome p)
					{
		super(sb, start1, start2, eb, end1, end2, option, p);

		links = new TreeSet<Link>();
		originalOverlap = Bundle.overlapT;		
		setParent(p, sb, eb);
		this.addLink(new Link(sb, start1, start2, eb, end1, end2, option, p));
	}
	
	Bundle(Link l){
		super(l.startBlock, l.start1, l.start2, l.endBlock, l.end1, l.end2, l.z, l.parent);
		links = new TreeSet<Link>();
		originalOverlap = Bundle.overlapT;
		setParent(l.parent, l.startBlock, l.endBlock);
		this.addLink(l);
	}
	
//	void reBundle(){
//		if (bundles == null) bundles = new TreeSet<Bundle>();
//		for(Link l : links){
//			if (bundles.size() == 0){
//				bundles.add(new Bundle(l));
//			}
//			else if(bundles.ceiling(new Bundle(l)) != null && bundles.ceiling(new Bundle(l)).compareTo(l)==0){
//				bundles.ceiling(new Bundle(l)).addLink(l);
//			}
//			else if(bundles.floor(new Bundle(l)) != null && bundles.floor(new Bundle(l)).compareTo(l) == 0){
//				bundles.floor(new Bundle(l)).addLink(l);
//			}
//			else{
//				bundles.add(new Bundle(l));
//			}
//		}
//	}
	//TODO the way the parent of blocks are being set is causing problems with the index and how bundles are 
	//being rendered...

    /**
     *
     * @param p
     * @param newStart
     * @param newEnd
     */
	void setParent(Genome p, int newStart, int newEnd){
		parent = p;
		startBlock = newStart;
		endBlock = newEnd;
		bpLength = parent.bpL;
		//internalPoints = new HashMap<Double, double[]>();
		if(overlapT!=originalOverlap){
			//reBundle();
		}
		//else bundles = null;
		setControl();
	}
	
	//minus the difference between the block's start and offset.

    /**
     *
     */
	public void setControl(){
		controlDistance = 0.2;
		theta[0][0] = bpLength * (start1 + parent.blockOffsets[startBlock] - (parent.blocks[startBlock].start - parent.blockOffsets[startBlock]));
		theta[0][2] = bpLength * (end2 + parent.blockOffsets[endBlock] - (parent.blocks[endBlock].start - parent.blockOffsets[endBlock]));
		//if the end is more than PI from the beginning (clockwise) 
		if(theta[0][2]-theta[0][0]>Math.PI||(theta[0][2]<theta[0][0] && theta[0][0]-theta[0][2]>Math.PI)) {
			theta[0][1] = (((2.0 * Math.PI) - theta[0][2] + theta[0][0]) / 2.0) + theta[0][2];
			linkLength = theta[0][1]-theta[0][2];
			linkLength = Math.PI - (linkLength % Math.PI);
			//controlDistance = theta[0][1] - theta[0][2];
		}
		else{
			theta[0][1] = bpLength * (0.5*((start1 + parent.blockOffsets[startBlock]- (parent.blocks[startBlock].start - parent.blockOffsets[startBlock]))+(end2+parent.blockOffsets[endBlock]- (parent.blocks[endBlock].start - parent.blockOffsets[endBlock]))));
			linkLength = bpLength *( end2 - start1);
			linkLength = (linkLength%Math.PI);
		}
		if(theta[0][1]>theta[0][0]) chordD = (1-Genome.BLOCK_WIDTH) * cos(theta[0][1]-theta[0][0]);
		else chordD = (1-Genome.BLOCK_WIDTH) * cos(theta[0][2] - theta[0][1]);
		//linkLength = bpLength *( start1 + blockOffsets[startBlock] + end2 + blockOffsets[endBlock]);

		//points for inner of arc. 
		controlDistance = chordD - 0.15;
		if (controlDistance>0.95) controlDistance = 0.95;
		if (controlDistance<0) controlDistance = 0;
		theta[1][0] = bpLength * (start2 + parent.blockOffsets[startBlock] - (parent.blocks[startBlock].start - parent.blockOffsets[startBlock]));
		theta[1][2] = bpLength * (end1 + parent.blockOffsets[endBlock]  - (parent.blocks[endBlock].start - parent.blockOffsets[endBlock]));
		if(theta[1][2]-theta[1][0]>Math.PI||(theta[1][2]<theta[1][0] && theta[1][0]-theta[1][2]>Math.PI)) 
			theta[1][1] = (((2.0 * Math.PI) - theta[1][2] + theta[1][0]) / 2.0) + theta[1][2];
		else	   
			theta[1][1] = bpLength * (0.5*((start2 + parent.blockOffsets[startBlock] - (parent.blocks[startBlock].start - parent.blockOffsets[startBlock]))+(end1+parent.blockOffsets[endBlock]  - (parent.blocks[endBlock].start - parent.blockOffsets[endBlock]))));

		//when parsing the links, set up an array of control points for the curves making up each edge of the link. 
		//control distance needs to be higher for shorter links. 
		// controlDistance = controlDistance/Math.PI;
		
		for(int i=0; i<2; i++){
			for(int j=0; j<9; j+=3){
				if(j==3) control[i][j] = controlDistance * sin(theta[i][j/3]);
				else control[i][j] = (1.0-Genome.BLOCK_WIDTH) * sin(theta[i][j/3]);
				if(j==3) control[i][j+1] = controlDistance * cos(theta[i][j/3]);
				else control[i][j+1] = (1.0-Genome.BLOCK_WIDTH) * cos(theta[i][j/3]);
				control[i][j+2] = 0.0;
			}
		}
	}

    /**
     *
     * @return
     */
	   public int renderMode(){
		   //System.out.println(Math.toDegrees(length * Block.bpLengthInRadians));
		   //This value has been tested at 800 pixels wide circle.        4775
		   if(Math.toDegrees(Math.abs(start2 - start1) * bpLength) > 0.15915) {
			   return GL2.GL_QUAD_STRIP;
		   }
		   else return GL.GL_LINE_STRIP;
	   }

    /**
     *
      * @param gl
     */
	public void render(GL2 gl) {
		if (bundles == null){
			//TODO this is where the bundle's colour is determined. 
		if (parent.blocks[startBlock].colour == null) parent.blocks[startBlock].colour = new Color((int) (random()*255), (int) (random()*255), (int) (random()*255));
            if(parent.gradient){
			    Color tempColor = ControlWindow.getColor(z);
			//System.out.println("z: " + z + " red: " + tempColor.getRed() + " green: " + tempColor.getGreen());
            if(parent.transparency)
			    gl.glColor4d((double) tempColor.getRed()/255,(double) tempColor.getGreen()/255, (double) tempColor.getBlue()/255, MAX_OPACITY * (z - parent.zDist.firstKey())/(parent.zDist.lastKey() - parent.zDist.firstKey()));
            else
                gl.glColor4d((double) tempColor.getRed()/255,(double) tempColor.getGreen()/255, (double) tempColor.getBlue()/255, MAX_OPACITY);
            }
            else{
                Color tempColor = parent.blocks[startBlock].colour;
                if(parent.transparency)
                    gl.glColor4d((double) tempColor.getRed()/255,(double) tempColor.getGreen()/255, (double) tempColor.getBlue()/255, MAX_OPACITY * (z - parent.zDist.firstKey())/(parent.zDist.lastKey() - parent.zDist.firstKey()));
                else
                    gl.glColor4d((double) tempColor.getRed()/255,(double) tempColor.getGreen()/255, (double) tempColor.getBlue()/255, MAX_OPACITY);
            }
			gl.glBegin(renderMode());
			//System.out.println("z = " + z);
			if(renderMode()==GL2.GL_QUAD_STRIP){
				Double midStart = Block.circlePoints.floorKey((theta[0][0] + theta[1][0])/2);
				Double midEnd = Block.circlePoints.ceilingKey((theta[0][0] + theta[1][0])/2);
				if(midStart == null){
					Block.getCirclePoint((theta[0][0] + theta[1][0])/2);
					midStart = Block.circlePoints.floorKey((theta[0][0] + theta[1][0])/2);
				}
				if(midEnd == null){
					Block.getCirclePoint((theta[0][0] + theta[1][0])/2);
					midEnd = Block.circlePoints.ceilingKey((theta[0][0] + theta[1][0])/2);
				}
				if(midStart<theta[0][0]&&midEnd>theta[1][0])
				while(midStart<theta[0][0]&&midEnd>theta[1][0]){
					gl.glVertex2d(Block.getCirclePoint(midStart)[2], Block.getCirclePoint(midStart)[3]);
					gl.glVertex2d(Block.getCirclePoint(midEnd)[2], Block.getCirclePoint(midEnd)[3]);
					midStart += Block.step*parent.bpL;
					midEnd -= Block.step*parent.bpL;
				}
				else{
					while(midStart>theta[0][0]&&midEnd<theta[1][0]){
						gl.glVertex2d(Block.getCirclePoint(midStart)[2], Block.getCirclePoint(midStart)[3]);
						gl.glVertex2d(Block.getCirclePoint(midEnd)[2], Block.getCirclePoint(midEnd)[3]);
						midStart -= Block.step*parent.bpL;
						midEnd += Block.step*parent.bpL;
				}
				}
			}
			
			for(int i=0; i<=splits; i++){
				double[] vert = getBezierPoint((double)i/splits);
				if(renderMode() == GL2.GL_QUAD_STRIP){
					gl.glVertex2d(vert[0], vert[1]);
					gl.glVertex2d(vert[2], vert[3]);
				}
				else{
					gl.glVertex2d(vert[0], vert[1]);
				}
			}
			if(renderMode()==GL2.GL_QUAD_STRIP){
				Double midStart = Block.circlePoints.floorKey((theta[0][2] + theta[1][2])/2);
				Double midEnd = Block.circlePoints.ceilingKey((theta[0][2] + theta[1][2])/2);
				if(midStart == null){
					Block.getCirclePoint((theta[0][2] + theta[1][2])/2);
					midStart = Block.circlePoints.floorKey((theta[0][2] + theta[1][2])/2);
				}
				if(midEnd == null){
					Block.getCirclePoint((theta[0][2] + theta[1][2])/2);
					midEnd = Block.circlePoints.ceilingKey((theta[0][2] + theta[1][2])/2);
				}
				if(midStart<theta[1][2]&&midEnd>theta[0][2])
				while(midStart<theta[1][2]&&midEnd>theta[0][2]){
					gl.glVertex2d(Block.getCirclePoint(midStart)[2], Block.getCirclePoint(midStart)[3]);
					gl.glVertex2d(Block.getCirclePoint(midEnd)[2], Block.getCirclePoint(midEnd)[3]);
					midStart += Block.step*parent.bpL;
					midEnd -= Block.step*parent.bpL;
				}
				else{
					while(midStart>theta[1][2]&&midEnd<theta[0][2]){
						gl.glVertex2d(Block.getCirclePoint(midStart)[2], Block.getCirclePoint(midStart)[3]);
						gl.glVertex2d(Block.getCirclePoint(midEnd)[2], Block.getCirclePoint(midEnd)[3]);
						midStart -= Block.step*parent.bpL;
						midEnd += Block.step*parent.bpL;
				}
				}
			}
			gl.glEnd();}
		//else for (Bundle b : bundles) b.render(gl);
		}

    /**
     *
     * @param d
     * @return
     */
	public double[] getBezierPoint(double d){
		//if(!internalPoints.containsKey(d)){
			double spx, spy, epx, epy;
			spx = bx(d, control[0]);
			spy = by(d, control[0]);
			epx = bx(d, control[1]);
			epy = by(d, control[1]);
			//internalPoints.put(d, new double[]{spx, spy, epx, epy});
		//}
		return new double[]{spx, spy, epx, epy};
	}
	
	
	@Override
/**
 *
 */
	public int compareTo(Link other) {
         if (this.orientation != other.orientation) {
             return -1;
         }
         else if (this.orientation == true){
             Link firstLink;
             Link secondLink;
             int returned;
             if (linkCompare(this, other)<0){   //   this link less than other.
                 firstLink = this;
                 secondLink = other;
                 returned = -1;
             }
             else if(linkCompare(this, other)>0){   //this link greater than other.
                 secondLink = this;
                 firstLink = other;
                 returned = 1;
             }
             else return 0;
             //secondLink.start1 - firstLink.start1 must be less than firstLink.width + half bundling threshold.
             if(secondLink.start1 - firstLink.start1 <= (firstLink.start2 - firstLink.start1) + (Bundle.overlapT /2)){
                if(secondLink.end1>=firstLink.end1){
                    if(secondLink.end1 - firstLink.end1 <= (secondLink.end2 - secondLink.end1) + (Bundle.overlapT / 2)&& (secondLink.end1 - firstLink.end2)>=0){
                        if(abs(firstLink.z - secondLink.z) < zThreshold)
                            return 0;
                        else return returned;
                    }
                    else return returned;
                }
                else return returned;
             }
             else return returned;
         }
         else{
             Link outsideLink;
             Link insideLink;
             int returned;
             if (linkCompare(this, other)<0){   //   this link less than other.
                 outsideLink = this;
                 insideLink = other;
                 returned = -1;
             }
             else if(linkCompare(this, other)>0){   //this link greater than other.
                 outsideLink = this;
                 insideLink = other;
                 returned = 1;
             }
             else return 0;
             if(insideLink.start1 - outsideLink.start1 <= (outsideLink.start2 - outsideLink.start1) + (Bundle.overlapT/2)){
                  if(insideLink.end2<=outsideLink.end2){
                        if((outsideLink.end1 - insideLink.end2) <= (outsideLink.end1 - outsideLink.end2) + (Bundle.overlapT/2) && (outsideLink.end1 - insideLink.end2)>=0){
                            if (abs(insideLink.z - outsideLink.z) <zThreshold)
                                return 0;
                            else return returned;
                        }
                        else return returned;
                  }
                  else return returned;
             }
             else return returned;
         }
	}

    /**
     *
     * @param t
     * @param other
     * @return
     */
    private int linkCompare(Link t, Link other){
        if(t.startBlock<other.startBlock) return -1;
        else if(t.startBlock>other.startBlock) return 1;
        else{
            if(t.endBlock<other.endBlock) return -1;
            else if(t.endBlock>other.endBlock) return 1;
            else{
                if(t.start1<other.start1) return -1;
                else if(t.start1>other.start1) return 1;
                else{
                    if(t.start2<other.start2) return -1;
                    else if(t.start2>other.start2) return 1;
                    else{
                        if(t.end1<other.end1) return -1;
                        else if(t.end1>other.end1) return 1;
                        else{
                            if(t.end2<other.end2) return -1;
                            else if(t.end2>other.end2) return 1;
                            else{
                                if(t.z<other.z) return -1;
                                else if(t.z>other.z) return 1;
                                else return 0;
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     *
     */
    @Override
	public String toString(){
		return "bundle size " + links.size();
	}
   // chr1 100 200 chr2 200 250 color=(107,174,241),thickness=2

    /**
     *
     * @return
     */
    public String toCircosString(){
        Color tempColor = ControlWindow.getColor(z);
         return (parent.blocks[startBlock].ID + " " + start1 + " " + start2 + " " + parent.blocks[endBlock].ID + " " + end1 + " " + end2 + " color=(" + tempColor.getRed() + "," + tempColor.getGreen() + ","
         + tempColor.getBlue() + "," + (1.0 - ((MAX_OPACITY * (z - parent.zDist.firstKey())/(parent.zDist.lastKey() - parent.zDist.firstKey())))) + ")");
    }

    /**
     * Reset the start and end points of a bundle when a link is added to it so that it is as wide as all the links it contains combined.
     * @param l is the link to be added.
     */
	public void addLink(Link l) {
		links.add(l);
        //First update start1 and start2:
        start1 = min(l.start1, this.start1);
        start2 = max(l.start2, this.start2);
        if (this.orientation){
            end1 = min(l.end1, this.end1);
            end2 = min(l.end2, this.end2);
        }
        else{
            end1 = max(l.end1, this.end1);
            end2 = min(l.end2, this.end2);
        }
        //Update the average Z value for the whole bundle.
		totalZ += l.z();
		z = totalZ / (double) links.size();
		this.setControl();
        //Keep track of how big the biggest bundle formed is.
		if (links.size()>biggestBundle) biggestBundle = links.size();
	}

    /**
     *
     * @param out
     * @param imagewidth
     */
	public void export(PrintWriter out, int imagewidth){
		int outerR = imagewidth/2;
		int innerR = imagewidth/2 - (int) (outerR*Genome.BLOCK_WIDTH);
		double sw  = 1.0;
		String fillColor = "rgb(" + parent.blocks[startBlock].colour.getRed() + ", " + parent.blocks[startBlock].colour.getGreen() + ", " + parent.blocks[startBlock].colour.getBlue() + ")";
		String p1, p2, p3, p4, c1, c2;
		if(renderMode() == GL2.GL_QUAD_STRIP){
			p1 = Integer.toString((int) (control[0][0] * outerR)) + " " + Integer.toString((int) (-control[0][1] * outerR));
			c1 = Integer.toString((int) (control[0][3] * outerR)) + " " + Integer.toString((int) (-control[0][4]* outerR)); 
			p2 = Integer.toString((int) (control[0][6] * outerR)) + " " + Integer.toString((int) (-control[0][7] * outerR));
			p3 = Integer.toString((int) (control[1][0] * outerR)) + " " + Integer.toString((int) (-control[1][1] * outerR));
			c2 = Integer.toString((int) (control[1][3] * outerR)) + " " + Integer.toString((int) (-control[1][4] * outerR)); 
			p4 = Integer.toString((int) (control[1][6] * outerR)) + " " + Integer.toString((int) (-control[1][7] * outerR));
			out.println("<path d=\"M "+ p1 + " Q " + c1 + " " + p2 + " A " + innerR + " " + innerR + " 1 0, 0 " + p4 +
					" Q " + c2 + " " + p3 + " A " + innerR + " " + innerR + " 1 0, 0 " + p1 + " z\" fill=\"" + fillColor + "\" stroke=\""+fillColor+"\" stroke-width=\"" + sw + "\" />");
		}
		else{
			p1 = Integer.toString((int) (control[0][0] * outerR)) + " " + Integer.toString((int) (control[0][1] * -outerR));
			c1 = Integer.toString((int) (control[0][3] * outerR)) + " " + Integer.toString((int) (control[0][4]* -outerR)); 
			p2 = Integer.toString((int) (control[0][6] * outerR)) + " " + Integer.toString((int) (control[0][7] * -outerR));
			out.println("<path d=\"M "+ p1 + " Q " + c1 + " " + p2 +  "\" fill=\"none\" stroke=\"" + fillColor + "\" stroke-width=\"" + sw + "\" />");
		}
	}
}
