package vis;

import java.awt.*;
import java.io.File;


import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.imageio.ImageIO;
import javax.media.opengl.awt.GLCanvas;


/**
 * JOGL 2.0 Program Template (GLCanvas)
 * This is the top-level "Container", which allocates and add GLCanvas ("Component")
 * and animator.
 */

public class Visualizer{
	InteractiveMediator mediator;
	Genome initialInput;
	GLUT glut;
	CircleView circleView;
	ControlWindow controls;
    Container buttonBox;
    MenuBar mbar;
    Menu menu;
	
   /** Constructor to setup the top-level container*/
   public Visualizer() {
      // Create the top-level container frame
	   //System.out.println("attached to canvas");
	  final Frame frame1 = new Frame("Circumspect");
	  frame1.setLayout(new BorderLayout());	   
	  //File chooser. 
	  final FileDialog fileDialog = new FileDialog(frame1);
	  fileDialog.setTitle("Choose a file to open");
       fileDialog.setVisible(true);
       File[] files = fileDialog.getFiles();
       long  currentTime = System.currentTimeMillis();
	  //TODO get file type. 
	  initialInput = new MizBeeParser(files[0]);
	  circleView = new CircleView(initialInput);
	  controls = new ControlWindow(initialInput);
      buttonBox = new Container();
      mbar = new MenuBar();
      menu = new Menu("File");
      mbar.add(menu);

	  mediator = new InteractiveMediator(frame1, circleView, circleView.window);
      menu.addActionListener(mediator);
	  mediator.giveControls(controls, controls.window);
      mediator.giveButtons(buttonBox);
      mediator.fileMenu = menu;

      circleView.window.setPreferredSize(new Dimension(CircleView.WINDOW_WIDTH, CircleView.WINDOW_HEIGHT));
	  controls.window.setPreferredSize(new Dimension(controls.WINDOW_WIDTH, controls.WINDOW_HEIGHT));

      frame1.setMenuBar(mbar);
      frame1.setLayout(new BorderLayout());
	  frame1.add(circleView.window, BorderLayout.WEST);
	  frame1.add(controls.window, BorderLayout.CENTER);
	  frame1.add(buttonBox, BorderLayout.EAST);
	 // System.out.println("attached the mediator to the frame and the cv.");
	  circleView.giveListener(mediator);
	  controls.giveListener(mediator);
      mediator.init();
	  frame1.pack();
	  //System.out.println("resized the frame");
	  System.out.println("set visible");
	  frame1.setVisible(true);
      System.out.println(System.currentTimeMillis() - currentTime);
       while(true) continue;
   }
 
   /** The entry main() method */
   public static void main(String[] args) {
            new Visualizer();  // run the constructor
   }
}
