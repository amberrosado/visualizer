package vis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
//TODO crashes with blank lines of input 
public class CircosParser extends Genome{
        BufferedReader conf; 
        ArrayList<String> blockReader;
        ArrayList<String> linkReaders;
        BufferedReader kary; 
        
        CircosParser(File fileName){
                super(fileName);
        }
        
        void setupInput(File fileName){
                linkReaders = new ArrayList<String>();
                File f = (File) fileName;
                String path = f.getPath();
                String name = f.getName();
                int index = path.indexOf(name);
                String ss = path.substring(0, index);
                try{
                        conf = new BufferedReader(new FileReader((File) fileName));
                        String line = conf.readLine();
                        while(! line.contains("karyotype")){
                                line = conf.readLine();
                        }
                        kary = new BufferedReader(new FileReader(ss + line.split("[=]")[1].trim()));
                        line = conf.readLine();
                        while (conf.ready()){
                                while (! line.equals("<link>")){
                                        if (conf.ready()) line = conf.readLine();
                                        else break;
                                }
                                if (conf.ready()){line = conf.readLine();
                                //BufferedReader link = new BufferedReader(new FileReader(line.split("[=]")[1].trim()));
                                linkReaders.add(ss + line.split("[=]")[1].trim());
                                //addLinks(link);
                                }
                        }
                }catch (Exception e){
                        System.out.println("die");
                }
        }
        
        Stack<Link> parseLinks() {
                String[] line;
                Stack<Link> foundLinks = new Stack<Link>();
                for(String b : linkReaders){
                	BufferedReader reader;
					try {
						reader = new BufferedReader(new FileReader(b));
	                	while(reader.ready()){
	                		line = reader.readLine().split("[ ]");
                            Link k = new Link(blockIDs.get(line[0]), Integer.parseInt(line[1]), Integer.parseInt(line[2]), blockIDs.get(line[3]),
                                    Integer.parseInt(line[4]), Integer.parseInt(line[5]), parseOptions(line[6]), this);
                            if(Math.abs(k.start1 - k.start2)<1000 && Math.abs(k.end1 - k.end2) < 1000)
	                		foundLinks.add(k);
	                	}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
                return foundLinks;
                }
        
        double parseOptions(String options){
        	double f = 1.0;
        	String[] items = options.split("[,]");
        	for(String s : items){
        		String[] t = s.split("[=]");
        		if(t[0].equals("color")){
        			
        		}
        		else if(t[0].equals("z")){ f = Math.abs(Double.parseDouble(t[1]));
        		}
        		else{ f = Double.parseDouble(t[0]);}
        	}
        	return f;
        }

            Block[] parseBlocks(){
                ArrayList<Block> tempblocks = new ArrayList<Block>();
                ArrayList<String> IDs = new ArrayList<String>();
                String[] line;
                Block[] zblocks = new Block[0];
                try {
                        while (kary.ready()){
                                line = kary.readLine().split("[ ]");
                                if(line[0].equals("chr")){
                                
                                //System.out.println("read line " + line);
                                IDs.add(line[2]);
                                tempblocks.add(IDs.indexOf(line[2]), (new Block(line[2], nbp, Integer.parseInt(line[5]) + nbp, nbp)));
                                tempblocks.get(IDs.indexOf(line[2])).setColor(colorMap.get(line[6]));
                                nbp += ( Integer.parseInt(line[5]));
                                System.out.println(nbp);}
                        }       
                        kary.close();
                }
                catch (IOException e) {

                }
                        zblocks = new Block[tempblocks.size()];
                        bpL = (Math.PI * 2.0)/(double) nbp;
                        for(int i= 0; i<zblocks.length; i++){
                                zblocks[i] = tempblocks.get(i);
                        }
                        tempblocks = null;
                
                return zblocks;
        }

}
