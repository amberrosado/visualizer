package vis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static java.lang.Math.*;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;

import com.jogamp.newt.event.WindowAdapter;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.gl2.GLUT;

//TODO This also needs to produce svg output? 
public class CircleView implements GLEventListener {
	static final int WINDOW_WIDTH = 990;  // width of the drawable
	static final int WINDOW_HEIGHT = 990; // height of the drawable
	//TODO what is the point of this class besides wrangling the openGL?
	static Genome visibleData;
	private Genome originalData;
	
	//the genome should render itself. 
	boolean filteredByLoc;

	GLJPanel window;
	GLU glu;
	GLUT glut;
	
	CircleView(Genome input){
		// Get the default OpenGL profile, reflecting the best for your running platform
		GLProfile glp = GLProfile.getDefault();
		// Specifies a set of OpenGL capabilities, based on your profile.
		GLCapabilities caps = new GLCapabilities(glp);
		// Create the OpenGL rendering canvas
		window = new GLJPanel(caps);
		//data setup
		originalData = input;
		visibleData = originalData;
		
		//further window setup
		window.addGLEventListener(this);
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	}
	
	public void filterData(){
		Genome newVisibleData = new InternalParser(visibleData);
		visibleData = newVisibleData;
	}
	
	public void giveListener(InteractiveMediator m){

	}


	/** Called back by the drawable to render OpenGL graphics */
	//When only direct interaction moves the scene the repaint-on-demand model should be used to save CPU time. 
	//The application calls GLAutoDrawable.display() manually at the end of the mouse or keyboard listener to cause repainting to be done!...
	@Override
	public void display(GLAutoDrawable drawable) {
	    GL2 gl = drawable.getGL().getGL2();
	    gl.glClear(GL.GL_COLOR_BUFFER_BIT);
		visibleData.render(gl, glut);

	}
	
	public void unFilter(){
		visibleData = originalData;
		visibleData.unFilter();
	}
	
	@Override
	public void init(GLAutoDrawable drawable) { 
		GL2 gl = drawable.getGL().getGL2();
		glu = new GLU();
		glut = new GLUT();
		int additionalWidth = WINDOW_WIDTH - WINDOW_HEIGHT;
		if (additionalWidth>0) additionalWidth = WINDOW_HEIGHT/additionalWidth;
		else additionalWidth = 1;
		//System.out.println(additionalWidth);
		glu.gluOrtho2D(-1.1, additionalWidth + 0.1, -1.1, 1.1);
		gl.glEnable(GL2.GL_POLYGON_SMOOTH);
		gl.glHint(GL2.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
		gl.glHint(GL2.GL_POLYGON_SMOOTH_HINT, GL.GL_NICEST);
		gl.glEnable(GL2.GL_POLYGON_SMOOTH_HINT);
		gl.glEnable(GL2.GL_LINE_SMOOTH_HINT);
		gl.glEnable(GL.GL_LINE_SMOOTH);
		gl.glEnable(GL2.GL_POLYGON_SMOOTH);
		gl.glEnable(GL.GL_BLEND);//GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		gl.glClearColor(1, 1, 1, 0);
		gl.glLineWidth(0.9f);
		gl.glPointSize(4.0f);
		filteredByLoc = false; 
	}

	/** Called back before the OpenGL context is destroyed. */
	@Override
	public void dispose(GLAutoDrawable drawable) { }

	/** Called back by the drawable when it is first set to visible,
	       and during the first repaint after the it has been resized. */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) { 
	}

	public void export(String string, int w) {
		try{
			//General setup of the output: header, image size etc. 
		PrintWriter out = new PrintWriter(new FileWriter(string.split("[.]")[0] + ".svg"));
		   out.println("<?xml version=\"1.0\" standalone=\"no\"?>");
		   out.println("<svg width=\"" + w + "px\" height=\"" + w + "px\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">");
		   out.println("<g transform=\"translate(" + w/2 + "," + w/2 + ")\">");
		   //visibleData.export(out, 800);
		   out.println("</g> </svg>");
		   out.close();
		   
		}
		catch(Exception e){
			
		}
	}

    public void circosExport(String fileName){
        try{
            PrintWriter out = new PrintWriter(new FileWriter(fileName));
            visibleData.circosExport(out);
            out.close();
        }
        catch(Exception e){

        }
    }

}