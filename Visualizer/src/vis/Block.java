package vis;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;

import static java.lang.Math.*;

import java.util.TreeMap;
import java.util.Iterator;
import java.util.TreeSet;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.gl2.GLUT;

public class Block{
	Genome parent;
	int totalBP = 0;
	int blockOffset;
	String ID;
	String label;
	int start;
	int end;
	int length;
	int nLinks = 0;
	
	//modify to be the quad tree. 
	TreeSet<Bundle> bundles = new TreeSet<Bundle>();
	Color colour;
	//for rendering the blocks. When there are multiple tracks, this needs to be recalculated for the radius of this track. 
	double bpLength;
	static TreeMap<Double, double[]> circlePoints = new TreeMap<Double, double[]>();
	static int divisions = 200;
	static int step;
	static double radius = 1.0;
	
	//Node root = new Node();
	boolean visible = true;
	boolean selected = false; 
	boolean filteredByLoc = false;
	boolean filteredByZ = false;

	Block(String name, int s, int e, int offset){
		ID = name;
		blockOffset = offset;
		start = s;
		end = e;
		//System.out.println(end);
		length = end - start;
		colour = new Color((float) random(), (float) random(), (float) random());
	}
	
	public void setColor(Color c){
		colour = c;
	}
	
	
	public boolean totalBP(int bp){
		totalBP = bp; 
		bpLength = (Math.PI * 2.0)/(double) bp;
		return true;
	}

	public int length(){
		return length;
	}
	
	//Links need to be comparable to bundles, a link is equal to a bundle if the link should be in the bundle. 
	//If the link is "in" bundles, get what bundles gives you when you ask to get the link, which will be a tree set of links, and add the link to that. 
	//A link is in a bundle if it meets some constraints. 
	//If the link is not in bundles, make a new bundle(Link l) and add that to bundles. 
	
	public boolean add(int sb, int start1, int start2, int eb, int end1, int end2, float option, double bpLength, int[] blockOffsets) {
		Bundle b;
		nLinks++;
			b = new Bundle(sb, start1, start2, eb, end1, end2, option, parent);
			if(!bundles.add(b)){
				Link l = new Link(sb, start1, start2, eb, end1, end2, option, parent);
				bundles.ceiling(b).addLink(l);
			}
			//System.out.println(bundles.size());
		//root.addLink(l);
		return true;
	}
	
	public double startT(){
		return blockOffset * bpLength;
	}
	
	public double endT(){
		return (blockOffset + length) * bpLength;
	}
	
	public void filterLinksByLocation(double start, double end){
		Iterator<Bundle> i = bundles.iterator();
		while(i.hasNext()){
			
		}
	}
	
	public void filterLinksByZ(double low, double high){
		
	}

	//Evaluators for getting points on a circle by angle
	public static double cx(double t, double r){
		return r * sin(t);
	}
	public static double cy(double t, double r){
		return r * cos(t);
	}

	public void render(GL2 gl, GLUT glut) {
		//Render the bundles
		//if((parent.selectedBlocks>0&&selected)||parent.selectedBlocks==0) for(Link l : bundles) l.render(gl);
		//Render the block. 
		step = totalBP/divisions;
		if (step<1) step = 1;
		//Render the inside of the block. 
		if (colour == null) colour = new Color((int) (random()*255), (int) (random()*255),(int) (random()*255));
		gl.glColor3d((double) (colour.getRed()/255.0), (double) (colour.getGreen()/255.0), (double) (colour.getBlue()/255.0));
		double theta = startT();
		gl.glBegin(GL2.GL_QUAD_STRIP);
		while(theta<=endT()){
			double[] outerpoint = getCirclePoint(theta);
			gl.glVertex2d(outerpoint[0], outerpoint[1]);
			gl.glVertex2d(outerpoint[2], outerpoint[3]);
			theta += step*bpLength;
		}
		double[] outerend = getCirclePoint(endT());
		gl.glVertex2d(outerend[0], outerend[1]);
		gl.glVertex2d(outerend[2], outerend[3]);
		gl.glEnd();
		gl.glFlush();
		//Draw the outlines
		gl.glColor3d(0.0, 0.0, 0.0);
		if (selected)
			gl.glColor3d(1.0, 1.0, 1.0);
		theta = startT();
		gl.glBegin(GL.GL_LINE_LOOP);
		while(theta<endT()){
			double[] point = getCirclePoint(theta);
			gl.glVertex2d(point[0], point[1]);
			theta += step * bpLength;
		}
		theta = endT();
		double[] point = getCirclePoint(theta);
		gl.glVertex2d(point[0], point[1]);
		while(theta>=startT()){
			point = getCirclePoint(theta);
			gl.glVertex2d(point[2],	point[3]);
			theta -= step * bpLength;
		}
		point = getCirclePoint(startT());
		gl.glVertex2d(point[2], point[3]);
		gl.glEnd();
		gl.glFlush();
		//TODO make this better. 
//		//setting up for rendering labels. the angle of the center of the block in degrees (for openGL)
//		theta = toDegrees((startT()*3 + endT())/4.0);
//		//sets the point where the next letter will be rendered.
//		gl.glPushMatrix();
//		gl.glColor3d(0.0001, 1.0, 0.0001);
//		point = getCirclePoint((startT()*3 + endT())/4.0);
//		//if (theta>180){
//		//	point[0] -= ((0.1 * (double) glut.glutStrokeLength(GLUT.STROKE_MONO_ROMAN, ID))/(double) CircleView.WINDOW_HEIGHT) * 2.0;
//		//}
//		//if (theta>90&&theta<270) point[1] -= ( (0.1 * (double) glut.glutStrokeWidth(GLUT.STROKE_MONO_ROMAN, 'X'))/(double) CircleView.WINDOW_HEIGHT) * 2.0;
//		gl.glTranslated(point[4], point[5], 0.0);
//		gl.glScaled(0.00015, 0.00015, 0.00015);
//		gl.glRotated(-theta, 0, 0, 1);
//		if(!ID.equals("Gap")) glut.glutStrokeString(GLUT.STROKE_MONO_ROMAN, ID);
//		gl.glPopMatrix();
	}
	// Link(int sb, int s1, int s2, int eb, int e1, int e2, float option, double bpL, int[] bOs, Block b) throws IOException{
    //TODO rewrite to be done by the genome.

//    public void export(PrintWriter out, int imagewidth){
//    	double sw = 1.0;
//    	//if((parent.selectedBlocks>0&&selected)||parent.selectedBlocks==0) for(Link b : bundles){
//    		//b.export(out, imagewidth);
//    	//}
//    	//this is the block
//    	String p1, p2, p3, p4, c1, c2;
//    	double[] a1, a2, a3, a4;
//    	int outerR = imagewidth/2;
//    	int innerR = (int) (outerR-(Genome.BLOCK_WIDTH * outerR));
//    	a1 = getCirclePoint(startT(), outerR);
//    	a2 = getCirclePoint(startT(), innerR);
//    	a3 = getCirclePoint(endT(), innerR);
//    	a4 = getCirclePoint(endT(), outerR);
//    	String fillColor = "rgb(" + colour.getRed() + ", " + colour.getGreen() + ", " +  colour.getBlue() + ")"; 
//    	p1 = (int) a1[0] + " " + (int) -a1[1];
//    	p2 = (int) a2[0] + " " + (int) -a2[1];
//    	p3 = (int) a3[0] + " " + (int) -a3[1];
//    	p4 = (int) a4[0] + " " + (int) -a4[1];
//    	if(endT()-startT()<Math.PI) out.println("<path d=\"M " + p2 + " A " + innerR + " " + innerR + " 0 0, 1 " + p3 + " L " + p4 + " A " + outerR + " " + outerR + " 0 0, 0 " +  p1 + " z\" fill=\"" + fillColor + "\" stroke=\"black\" stroke-width=\"" + sw + "\" />");
//    	else out.println("<path d=\"M " + p2 + " A " + innerR + " " + innerR + " 0 1, 1 " + p3 + " L " + p4 + " A " + outerR + " " + outerR + " 0 1, 0 " +  p1 + " z\" fill=\"" + fillColor + "\" stroke=\"black\" stroke-width=\"" + sw + "\" />");
//    	
//    }
	static double[] getCirclePoint(double theta) {
		if(! circlePoints.containsKey(theta)){
			circlePoints.put(theta, new double[]{cx(theta, radius), cy(theta, radius), cx(theta, radius-Genome.BLOCK_WIDTH), cy(theta, radius-Genome.BLOCK_WIDTH), cx(theta, radius+0.012), cy(theta, radius + 0.012)});
		}
		return circlePoints.get(theta);
	}

    static double[] getCirclePoint(double theta, double radius){
        return new double[]{cx(theta, radius), cy(theta, radius)};
    }

	public void exportRegion(PrintWriter out, double s, double e, int width) {
		for (Bundle b : bundles){
			if(b.theta[0][0]>=s && b.theta[0][0]<= e) b.export(out, width);
		}
	}
}
