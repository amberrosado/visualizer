package vis;

import java.io.*;
import java.util.Stack;


public class BlastNParser extends Genome {
    BufferedReader fileReader;

    BlastNParser(File fileName){
        super(fileName);
    }
    @Override
    void setupInput(File fileName) {
        try{
            fileReader = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    Block[] parseBlocks() {
        try{
        String line = fileReader.readLine();
        while (! line.split("[=]")[0].equals("Query")) line = fileReader.readLine();
        }catch(IOException e){

            }
        return new Block[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    Stack<Link> parseLinks() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
